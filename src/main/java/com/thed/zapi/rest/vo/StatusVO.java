package com.thed.zapi.rest.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by smangal on 2/1/14.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StatusVO {
    @XmlElement(nillable=true)
    public String id;

    @XmlElement(nillable=true)
    public String color;

    @XmlElement(nillable=true)
    public String description;

    @XmlElement(nillable=true)
    public String name;

    public String type;

}
