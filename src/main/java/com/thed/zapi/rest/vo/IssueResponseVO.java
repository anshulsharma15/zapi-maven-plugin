package com.thed.zapi.rest.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by smangal on 2/2/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IssueResponseVO {
    public Long id;
    public String key;
    public String self;
}
