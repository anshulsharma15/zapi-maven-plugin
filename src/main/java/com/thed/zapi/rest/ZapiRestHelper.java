package com.thed.zapi.rest;

import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.*;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.thed.zapi.rest.config.Configuration;
import com.thed.zapi.rest.generated.ZapiRestClient;
import com.thed.zapi.rest.vo.*;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.text.DateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.thed.zapi.rest.generated.ZapiRestClient.*;

/**
 * Created by smangal on 1/30/14.
 */
public class ZapiRestHelper {

    private final com.sun.jersey.api.client.Client client;
    private final Configuration config;
    final static Logger logger = Logger.getLogger(ZapiRestHelper.class.getName());

    public ZapiRestHelper(Configuration config) {
        this.config = config;
        ClientConfig cc = new DefaultClientConfig();
        cc.getClasses().add(JacksonJsonProvider.class);
        client = Client.create(cc);
        logger.logInfo("Created Cliuent....."+ client);
        final HTTPBasicAuthFilter authFilter = new HTTPBasicAuthFilter(config.getUserName(), config.getPassword());
        client.addFilter(authFilter);
        client.addFilter(new ClientFilter() {
            private ArrayList<Object> cookies;

            @Override
            public ClientResponse handle(ClientRequest request) throws ClientHandlerException {
                if (cookies != null) {
                    request.getHeaders().put("Cookie", cookies);
                }
                ClientResponse response = getNext().handle(request);
                if (response.getCookies() != null) {
                    if (cookies == null) {
                        cookies = new ArrayList<Object>();
                    }
                    // simple addAll just for illustration (should probably check for duplicates and expired cookies)
                    cookies.addAll(response.getCookies());
                }
                return response;
            }
        });
        String zapiInfo = ZapiRestClient.systemInfo(client, config.getZapiUrl()).getAsJson(String.class);
        logger.info(zapiInfo);
        client.removeFilter(authFilter);
    }

    /**
     * Creates a new Cycle
     * @return
     * @throws JSONException
     * @param cyclePrefix
     */
    public Long createCycle(String cyclePrefix) throws JSONException {
    	logger.logInfo("Entered in create Cycle...... ");
        Cycle cycle = cycle(client, config.getZapiUrl());
        logger.logInfo("Completed creating cycle from Cycle  ...... ");
        //JSONObject cycleJSON = new JSONObject(ImmutableMap.of("name", "Cycle" + new Date(), "projectId", config.getProjectId(), "versionId", config.getVersionId(), "description","Creation from ZAPI Maven Plugin"));
        CycleVO cycleObj = new CycleVO(null, null, cyclePrefix + " " + DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT).format(new Date()), "Build", "Created from ZAPI Maven Plugin", "Env", config.getVersionId(), config.getProjectId(), null, null);
        GenericResponseVO cResult = cycle.postJson(cycleObj, new GenericType<GenericResponseVO>() {});
        return cResult.getId();
    }

    public void deleteCycle(Long id) throws JSONException {
        Cycle cycle = cycle(client, config.getZapiUrl());
        /*{"success":"Cycle 9 successfully deleted"}*/
        String cResult = cycle.id(id).deleteAsJson(String.class);
        logger.log(Level.ALL, cResult);
    }

    public void executeTest(Integer cycleId, Integer testcaseId, String result, String stackTrace) throws JSONException, IOException {
        Execution executionService = execution(client, config.getZapiUrl());
        ExecutionResponseVO executionRes = executionService.getAsJson(testcaseId, null, null, cycleId, 0, null, null, ExecutionResponseVO.class);
        String updateResponse;
        ExecutionVO executionObj;
        if(executionRes != null && executionRes.recordsCount > 0){
            executionObj = executionRes.executions.get(0);
        }else{
            executionObj = new ExecutionVO(result, cycleId, config.getProjectId(), config.getVersionId(), testcaseId.toString(), stackTrace);
            Map<String, ExecutionVO> executionVOMap = executionService.postJson(executionObj, new GenericType<Map<String, ExecutionVO>>(){});
            executionObj = executionVOMap.entrySet().iterator().next().getValue();
        }
        executionObj.executionStatus = result;
        stackTrace = StringUtils.substring(stackTrace, 0, 750);
        Map<String, String> updateVals = ImmutableMap.<String, String>of("status", result, "comment", stackTrace);
        updateResponse = executionService.idExecute(executionObj.id).putJson(new ObjectMapper().writeValueAsString(updateVals), String.class);
        logger.log(Level.ALL, updateResponse);
    }

    public List<StatusVO> getExecutionStatuses(){
        Util.TestExecutionStatus statusUtil = util(client, config.getZapiUrl()).testExecutionStatus();
        List<StatusVO> value = statusUtil.getAsJson(new GenericType<List<StatusVO>>() {});
        return value;
    }

    public List<StatusVO> getStepStatuses(){
        Util.TeststepExecutionStatus statusUtil = util(client, config.getZapiUrl()).teststepExecutionStatus();
        List<StatusVO> value = statusUtil.getAsJson(new GenericType<List<StatusVO>>() {});
        return value;
    }

    public Long findOrAddTestcase(String fieldName, String fieldVal) throws JSONException, IOException {
        String issueRes = getAsJson(new GenericType<String>(){}, config.getJiraUrl(), "search", ImmutableMap.<String, Object>of("jql", fieldName + "~\"" + fieldVal +"\""));
        JSONArray issueObjects = new JSONObject(issueRes).getJSONArray("issues");
        if(issueObjects != null && issueObjects.length() > 0){
            return issueObjects.getJSONObject(0).getLong("id");
        }else{
            Map<String, Object> fields = ImmutableMap.<String, Object>of("project", ImmutableMap.of("id", config.getProjectId()), fieldName, fieldVal, "issuetype", ImmutableMap.of("name", "Test"));
            Map<String, Object> issueMap = ImmutableMap.<String, Object>of("fields", fields);
            final String input = new ObjectMapper().writeValueAsString(issueMap);
            IssueResponseVO issue = postJson(new GenericType<IssueResponseVO>(){}, config.getJiraUrl(), "issue", input);
            return issue.id;
        }
    }

    private<T >T postJson(com.sun.jersey.api.client.GenericType<T> returnType, URI basePath, String apiName, Object input) {
        HashMap<String,Object> _templateAndMatrixParameterValues = new HashMap<String, Object>();
        UriBuilder localUriBuilder = UriBuilder.fromUri(basePath).path(apiName);
        com.sun.jersey.api.client.WebResource resource = client.resource(localUriBuilder.buildFromMap(_templateAndMatrixParameterValues));
        com.sun.jersey.api.client.WebResource.Builder resourceBuilder = resource.getRequestBuilder();
        resourceBuilder = resourceBuilder.accept("application/json");
        resourceBuilder = resourceBuilder.type("application/json");
        com.sun.jersey.api.client.ClientResponse response;
        response = resourceBuilder.method("POST", com.sun.jersey.api.client.ClientResponse.class, input);
        if (response.getStatus()>= 400) {
            throw new WebApplicationException(Response.status(response.getClientResponseStatus()).build());
        }
        return response.getEntity(returnType);
    }

    private<T >T getAsJson(com.sun.jersey.api.client.GenericType<T> returnType, URI basePath, String apiName, Map<String, Object> queryParams) {
        HashMap<String,Object> _templateAndMatrixParameterValues = new HashMap<String, Object>();
        UriBuilder localUriBuilder = UriBuilder.fromUri(basePath).path(apiName);
        for (Map.Entry<String, Object> params : queryParams.entrySet()) {
            localUriBuilder.replaceQueryParam(params.getKey(), params.getValue());
        }

        com.sun.jersey.api.client.WebResource resource = client.resource(localUriBuilder.buildFromMap(_templateAndMatrixParameterValues));
        com.sun.jersey.api.client.WebResource.Builder resourceBuilder = resource.getRequestBuilder();
        resourceBuilder = resourceBuilder.accept("application/json");
        com.sun.jersey.api.client.ClientResponse response;
        response = resourceBuilder.method("GET", com.sun.jersey.api.client.ClientResponse.class);
        if (response.getStatus()>= 400) {
            throw new WebApplicationException(Response.status(response.getClientResponseStatus()).build());
        }
        return response.getEntity(returnType);
    }
}
