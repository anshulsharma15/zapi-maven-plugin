package com.thed.zapi.rest.config;

import java.net.URI;

/**
 * Created by smangal on 1/30/14.
 */
public class Configuration {
    private String baseUrl;
    private URI zapiUrl;
    private URI jiraUrl;
    private String userName;
    private String password;

    private int cyclePolicy;
    private String projectId;
    private String versionId;

    public Configuration(String url, String userName, String password, int cyclePolicy, String projectId, String versionId) {
        this.baseUrl = url;
        this.zapiUrl = URI.create(url + "rest/zapi/latest");
        this.jiraUrl = URI.create(url + "rest/api/latest");
        this.userName = userName;
        this.password = password;
        this.cyclePolicy = cyclePolicy;
        this.projectId = projectId;
        this.versionId = versionId;
    }

    public URI getZapiUrl() {
        return zapiUrl;
    }
    public URI getJiraUrl() {
        return jiraUrl;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getVersionId() {
        return versionId;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "baseUrl='" + baseUrl + '\'' +
                ", zapiUrl=" + zapiUrl +
                ", jiraUrl=" + jiraUrl +
                ", userName='" + userName + '\'' +
                ", password='******'" +
                ", cyclePolicy=" + cyclePolicy +
                ", projectId='" + projectId + '\'' +
                ", versionId='" + versionId + '\'' +
                '}';
    }
}
