package com.thed.zapi.mojo;

import java.io.*;
import java.net.MalformedURLException;
import java.util.*;

import com.thed.zapi.rest.config.Configuration;
import com.thed.zapi.rest.ZapiRestHelper;
import com.thed.zapi.rest.vo.StatusVO;
import org.apache.maven.doxia.sink.render.RenderingContext;
import org.apache.maven.doxia.site.decoration.DecorationModel;
import org.apache.maven.doxia.siterenderer.Renderer;
import org.apache.maven.doxia.siterenderer.RendererException;
import org.apache.maven.doxia.siterenderer.SiteRenderingContext;
import org.apache.maven.doxia.siterenderer.sink.SiteRendererSink;
import org.apache.maven.model.ReportPlugin;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.surefire.report.ReportTestCase;
import org.apache.maven.plugins.surefire.report.ReportTestSuite;
import org.apache.maven.plugins.surefire.report.SurefireReportParser;
import org.apache.maven.project.MavenProject;
import org.apache.maven.reporting.AbstractMavenReport;
import org.apache.maven.reporting.MavenReportException;
import org.apache.maven.shared.utils.PathTool;
import org.apache.maven.shared.utils.StringUtils;
import org.codehaus.jettison.json.JSONException;

/**
 * Abstract base class for reporting test results using zapi-maven-plugin.
 *
 * @author Zephyr Dev
 */
public abstract class AbstractZapiReportMojo extends AbstractMavenReport {
    /**
     * Location where generated html will be created.
     *
     * @noinspection UnusedDeclaration
     */
    @Parameter(property = "project.reporting.outputDirectory")
    private File outputDirectory;

    /**
     * Maven Project
     *
     * @noinspection UnusedDeclaration
     */
    @Parameter(property = "project", required = true, readonly = true)
    private MavenProject project;

    /**
     * If set to false, only failures are shown.
     *
     * @noinspection UnusedDeclaration
     */
    @Parameter(defaultValue = "true", required = true, property = "showSuccess")
    private boolean showSuccess;

    /**
     * Directories containing the XML Report files that will be parsed and rendered to HTML format.
     *
     * @noinspection UnusedDeclaration
     */
    @Parameter
    private File[] reportsDirectories;

    /**
     * (Deprecated, use reportsDirectories) This directory contains the XML Report files that will be parsed and rendered to HTML format.
     *
     * @noinspection UnusedDeclaration
     */
    @Deprecated
    @Parameter
    private File reportsDirectory;

    /**
     * The projects in the reactor for aggregation report.
     *
     * @noinspection MismatchedQueryAndUpdateOfCollection, UnusedDeclaration
     */
    @Parameter(property = "reactorProjects", readonly = true)
    private List<MavenProject> reactorProjects;

    /**
     * Location of the Xrefs to link.
     *
     * @noinspection UnusedDeclaration
     */
    @Parameter(defaultValue = "${project.reporting.outputDirectory}/xref-test")
    private File xrefLocation;

    /**
     * Whether to link the XRef if found.
     *
     * @noinspection UnusedDeclaration
     */
    @Parameter(defaultValue = "true", property = "linkXRef")
    private boolean linkXRef;

    /**
     * Whether to build an aggregated report at the root, or build individual reports.
     *
     * @noinspection UnusedDeclaration
     */
    @Parameter(defaultValue = "false", property = "aggregate")
    private boolean aggregate;
    private Log logger;

    /**
     * Whether the report should be generated or not.
     *
     * @return {@code true} if and only if the report should be generated.
     * @since 2.11
     */
    protected boolean isSkipped() {
        return false;
    }

    /**
     * Whether the report should be generated when there are no test results.
     *
     * @return {@code true} if and only if the report should be generated when there are no result files at all.
     * @since 2.11
     */
    protected boolean isGeneratedWhenNoResults() {
        return false;
    }

    @Parameter(defaultValue = "http://localhost:2990/jira/", property = "zapi.url")
    private String url;

    @Parameter(defaultValue = "admin", property = "zapi.userName")
    private String userName;

    @Parameter(defaultValue = "admin", property = "zapi.password")
    private String password;

    @Parameter(defaultValue = "10000", property = "zapi.projectId")
    private String projectId;

    @Parameter(defaultValue = "10000", property = "zapi.versionId")
    private String versionId;

    @Parameter(defaultValue = "Cycle", property = "zapi.cyclePrefix")
    private String cyclePrefix;

    @Parameter(defaultValue = "1", property = "zapi.cycleCreationPolicy")
    private Integer cycleCreationPolicy;

    public String getUrl() {
        return url;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getVersionId() {
        return versionId;
    }

    public int getCycleCreationPolicy() {
        return cycleCreationPolicy;
    }

    public void setLog(Log log){
        logger = log;
    }

    /**
     * {@inheritDoc}
     */
    public void executeReport(Locale locale)
            throws MavenReportException {
        logger.info("Inside ZAPI");
        System.out.println("ZAPI - the old way");
        if (isSkipped()) {
            return;
        }

        final List<File> reportsDirectoryList = getReportsDirectories();

        if (reportsDirectoryList == null) {
            return;
        }

        if (!isGeneratedWhenNoResults()) {
            boolean atLeastOneDirectoryExists = false;
            for (Iterator<File> i = reportsDirectoryList.iterator(); i.hasNext() && !atLeastOneDirectoryExists; ) {
                atLeastOneDirectoryExists = SurefireReportParser.hasReportFiles(i.next());
            }
            if (!atLeastOneDirectoryExists) {
                return;
            }
        }
        logger.info("Inside ZAPI, Building Configuration");
        Configuration config = new Configuration(getUrl(), getUserName(), getPassword(), getCycleCreationPolicy(), getProjectId(), getVersionId());
        logger.info("Config is " + config);
        ZapiRestHelper zapiHelper = new ZapiRestHelper(config);
        try {
            Long cycleId = zapiHelper.createCycle(cyclePrefix);

            SurefireReportParser report = new SurefireReportParser(reportsDirectoryList, locale);
            List<ReportTestSuite> suites = report.parseXMLReportFiles();
            List<StatusVO> statuses = zapiHelper.getExecutionStatuses();
            StatusVO passedStatusVO = null;
            StatusVO failedStatusVO = null;
            for(StatusVO status : statuses){
                if(StringUtils.contains(status.name, "FAIL"))
                    failedStatusVO = status;
                if(StringUtils.contains(status.name, "PASS"))
                    passedStatusVO = status;
            }
            for (ReportTestSuite suite : suites) {
                for (ReportTestCase tc : suite.getTestCases()) {
                    Long testcaseId = zapiHelper.findOrAddTestcase("summary", tc.getFullName());
                    String statusId = (tc.getFailure() != null && tc.getFailure().size() > 0) ? failedStatusVO.id : passedStatusVO.id;
                    String comments = (tc.getFailure() != null && tc.getFailure().size() > 0) ? mapToString(tc.getFailure()) : " Successfully Executed in " + tc.getTime() + " sec";
                    //logger.info("comments " + comments);
                    zapiHelper.executeTest(cycleId.intValue(), testcaseId.intValue(), statusId, comments);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("Inside ZAPI, Done with uploading ");

//        SurefireReportGenerator report =
//                new SurefireReportGenerator( reportsDirectoryList, locale, showSuccess, determineXrefLocation() );
//
//        report.doGenerateReport( getBundle( locale ), getSink() );
    }

    /**
     * Converts map into readable String
     * @param failures
     * @return
     */
    private String mapToString(Map<String, Object> failures) {
        StringBuffer failureString = new StringBuffer();
        for(Map.Entry<String, Object> failure : failures.entrySet()){
            failureString.append(failure.getKey()).append(":").append(failure.getValue()).append("\n");
        }
        return failureString.toString();
    }

    public boolean canGenerateReport() {
        if (isSkipped()) {
            return false;
        }

        final List<File> reportsDirectoryList = getReportsDirectories();

        if (reportsDirectoryList == null) {
            return false;
        }

        if (!isGeneratedWhenNoResults()) {
            boolean atLeastOneDirectoryExists = false;
            for (Iterator<File> i = reportsDirectoryList.iterator(); i.hasNext() && !atLeastOneDirectoryExists; ) {
                atLeastOneDirectoryExists = SurefireReportParser.hasReportFiles(i.next());
            }
            if (!atLeastOneDirectoryExists) {
                return false;
            }
        }

        return super.canGenerateReport();
    }

    private List<File> getReportsDirectories() {
        final List<File> reportsDirectoryList = new ArrayList<File>();

        if (reportsDirectories != null) {
            reportsDirectoryList.addAll(Arrays.asList(reportsDirectories));
        }
        //noinspection deprecation
        if (reportsDirectory != null) {
            //noinspection deprecation
            reportsDirectoryList.add(reportsDirectory);
        }
        if (aggregate) {
            if (!project.isExecutionRoot()) {
                return null;
            }
            if (reportsDirectories == null) {
                for (MavenProject mavenProject : getProjectsWithoutRoot()) {
                    reportsDirectoryList.addAll(getSurefireReportsDirectory(mavenProject));
                }
            } else {
                // Multiple report directories are configured.
                // Let's see if those directories exist in each sub-module to fix SUREFIRE-570
                String parentBaseDir = getProject().getBasedir().getAbsolutePath();
                for (MavenProject subProject : getProjectsWithoutRoot()) {
                    String moduleBaseDir = subProject.getBasedir().getAbsolutePath();
                    for (File reportsDirectory1 : reportsDirectories) {
                        String reportDir = reportsDirectory1.getPath();
                        if (reportDir.startsWith(parentBaseDir)) {
                            reportDir = reportDir.substring(parentBaseDir.length());
                        }
                        File reportsDirectory = new File(moduleBaseDir, reportDir);
                        if (reportsDirectory.exists() && reportsDirectory.isDirectory()) {
                            getLog().debug("Adding report dir : " + moduleBaseDir + reportDir);
                            reportsDirectoryList.add(reportsDirectory);
                        }
                    }
                }
            }
        } else {
            if (reportsDirectoryList.size() == 0) {

                reportsDirectoryList.addAll(getSurefireReportsDirectory(project));
            }
        }
        return reportsDirectoryList;
    }

    /**
     * Gets the default surefire reports directory for the specified project.
     *
     * @param subProject the project to query.
     * @return the default surefire reports directory for the specified project.
     */
    protected abstract List<File> getSurefireReportsDirectory(MavenProject subProject);

    private List<MavenProject> getProjectsWithoutRoot() {
        List<MavenProject> result = new ArrayList<MavenProject>();
        for (MavenProject subProject : reactorProjects) {
            if (!project.equals(subProject)) {
                result.add(subProject);
            }
        }
        return result;

    }

    private String determineXrefLocation() {
        String location = null;

        if (linkXRef) {
            String relativePath = PathTool.getRelativePath(getOutputDirectory(), xrefLocation.getAbsolutePath());
            if (StringUtils.isEmpty(relativePath)) {
                relativePath = ".";
            }
            relativePath = relativePath + "/" + xrefLocation.getName();
            if (xrefLocation.exists()) {
                // XRef was already generated by manual execution of a lifecycle binding
                location = relativePath;
            } else {
                // Not yet generated - check if the report is on its way
                for (Object o : project.getReportPlugins()) {
                    ReportPlugin report = (ReportPlugin) o;

                    String artifactId = report.getArtifactId();
                    if ("maven-jxr-plugin".equals(artifactId) || "jxr-maven-plugin".equals(artifactId)) {
                        location = relativePath;
                    }
                }
            }

            if (location == null) {
                getLog().warn("Unable to locate Test Source XRef to link to - DISABLED");
            }
        }
        return location;
    }

    /**
     * {@inheritDoc}
     */
    public String getName(Locale locale) {
        return getBundle(locale).getString("report.surefire.name");
    }

    /**
     * {@inheritDoc}
     */
    public String getDescription(Locale locale) {
        return getBundle(locale).getString("report.surefire.description");
    }

    /**
     * {@inheritDoc}
     */
    protected Renderer getSiteRenderer() {
        return new Renderer(){

            @Override
            public void render(Collection collection, SiteRenderingContext siteRenderingContext, File file) throws RendererException, IOException {}

            @Override
            public void generateDocument(Writer writer, SiteRendererSink siteRendererSink, SiteRenderingContext siteRenderingContext) throws RendererException {}

            @Override
            public SiteRenderingContext createContextForSkin(File file, Map map, DecorationModel decorationModel, String s, Locale locale) throws IOException {
                return null;
            }

            @Override
            public SiteRenderingContext createContextForTemplate(File file, File file2, Map map, DecorationModel decorationModel, String s, Locale locale) throws MalformedURLException {
                return null;
            }

            @Override
            public void copyResources(SiteRenderingContext siteRenderingContext, File file, File file2) throws IOException {}

            @Override
            public Map locateDocumentFiles(SiteRenderingContext siteRenderingContext) throws IOException, RendererException {
                return null;
            }

            @Override
            public void renderDocument(Writer writer, RenderingContext renderingContext, SiteRenderingContext siteRenderingContext) throws RendererException, FileNotFoundException, UnsupportedEncodingException {}
        };
    }

    /**
     * {@inheritDoc}
     */
    protected MavenProject getProject() {
        return project;
    }

    /**
     * {@inheritDoc}
     */
    public abstract String getOutputName();

    /**
     * {@inheritDoc}
     */
    protected String getOutputDirectory() {
        return outputDirectory.getAbsolutePath();
    }

    private ResourceBundle getBundle(Locale locale) {
        return ResourceBundle.getBundle("surefire-report", locale, this.getClass().getClassLoader());
    }
}